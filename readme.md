# JAVA AUTOMATION TUTORIAL
### JVM + CUCUMBER + SELENIUM
HENRIQUE DALCOMUNE  
hdalcomune@avenuecode.com

## Table of Contents  
[Getting Started](#getting-started)  
[Requirements](#requirements)  
[Creating your Cucumber project](#creating-your-cucumber-project)  
[Creating our first test](#creating-our-first-test)  
[Creating the Test Runner](#creating-the-test-runner)  
[The Feature File](#the-feature-file)  
[Creating the Step Definitions](#creating-the-step-definitions)  
[Page Object Model](#page-object-model)  
[Exercises](#exercises)

## Getting Started
This document is an introduction to test automation using Selenium, Cucumber and Page Object Model in Java.

## Requirements
* Install the latest version of Java Development Kit, you can download from Oracle website.
* Also install Maven, on Mac you can also install it using Brew package manager. You can check the Java and Maven versions by typing: `mvn --version`.
* Install Eclipse Mars.2 or greater, although you should also be able to do everything using VIM.
* Install Chromedriver 
	- On mac you can use `brew install chromedriver`
	- On Linux follow this tutorial: <https://developers.supportbee.com/blog/setting-up-cucumber-to-run-with-Chrome-on-Linux/>
	- On Windows download chromedriver and place on your system path - <https://sites.google.com/a/chromium.org/chromedriver/downloads>
* After installing everything you are good to go and follow the next topics.

## Creating your Cucumber project
First, let's start by creating a new project. Start Eclipse then create a new project, select a Maven Project as shown below:

![alt text](images/1-wizard.png)

On the next window, choose to create a Maven Project without an archetype.

![alt text](images/2-wizard-project.png)

To finish, we need to set a GroupID, Artifact ID and a Name for our project. Where name should be the name of your project.

![alt text](images/3-wizard-project-conf.png)

After creating your project it should be organized with the following structure:

![alt text](images/4-project-structure.png)

All the project configuration we chose is stored inside the pom.xml file which is located in the root folder of your project. This file contains all the information necessary to maven to build your project. After the project is created you can still change some information in the pom file like the name, groupId, artifactId, url and description. For more info on what can be changed see [Maven's POM Reference](https://maven.apache.org/pom.html).  

Now, we need to add the dependencies to our pom file, let's take a look one each one before moving forward. For our project we'll need:
* cucumber-java - this is what defines the language that we'll use to code the cucumber steps.
* cucumber-junit - it is the runner module that gives access to cucumber command line and in this case the access to JUnit test API. cucumber-testng can also be used instead.
* selenium-java - this is the dependency to add the Selenium WebDriver to the project.

Let's go ahead and make the changes to our file, open your pom.xml file. We can either edit the xml file or manage it using Eclipse, let's go with the second option for now. Click the Dependencies tab then click Add.

![alt text](images/5-project-dependencies.png)

Add all three dependencies to the project.

| GroupID       | ArtifactID              | Version   |
| ------------- |:-------------:          | -----:    |
| info.cukes    | cucumber-java           | 1.2.4     |
| info.cukes    | cucumber-junit          | 1.2.4     |
| org.seleniumhq.selenium | selenium-java | 2.53.0    |

At the end if you switch the pom file to xml view you should see the dependencies organized like shown below:

```xml
<dependencies>
  	<dependency>
  		<groupId>info.cukes</groupId>
  		<artifactId>cucumber-java</artifactId>
  		<version>1.2.4</version>
  	</dependency>
  	<dependency>
  		<groupId>info.cukes</groupId>
  		<artifactId>cucumber-junit</artifactId>
  		<version>1.2.4</version>
  	</dependency>
  	<dependency>
  		<groupId>org.seleniumhq.selenium</groupId>
  		<artifactId>selenium-java</artifactId>
  		<version>2.53.0</version>
  	</dependency>
  </dependencies>
```

If you switch to the Dependency Hierarchy tab you will notice that all the dependencies have already been resolved. That's the advantage of using Maven, we don't need to keep adding libraries and dependencies to the project.

Now to finish the configuration part, let's do a cleanup in our project's structure. Remove the source folders `src/main/java`, `src/main/resources`. After that, we'll need to create the packages that will hold our code. We'll not go into details but consider the packages as a container for a group of classes and interfaces.

Create two packages, both name 'com.avenuecode' under 'src/test/java' and 'src/test/resources'. The project structure should look like this:

![alt text](images/6-project-structure.png)

If you want the read more about packages, check http://docs.oracle.com/javase/tutorial/java/package/packages.html.

## Creating our first test

Our test objective will be Avenue Code website. Let's make sure the Careers page display six job locations. The automation should go to the website, navigate to Careers page and validate the number of locations available.
The website address is http://www.avenuecode.com, the Career Page URL is http://site-staging.avenuecode.com/careers.


## Creating the Test Runner

In order to execute the tests we need to create a test runner, that is basically a class where we need to inform JUnit that the tests use Cucumber. Create a TestRunner class and start adding the following code:

```java
package com.avenuecode;

import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

public class TestRunner {}
```

Import Cucumber and JUnit dependencies. Finally, let's configure  JUnit to use Cucumber. That we can do by adding the following Annotation (Line #6):

```java
package com.avenuecode;

import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)

public class TestRunner {}
```

Now we want to configure Cucumber to generate our HTML report. Unlike Ruby, in Java, there's no yml file to add our configuration. Change your code again by importing the CucumberOptions and setting up the configuration by adding another Annotation:

```java
package com.avenuecode;

import cucumber.api.junit.Cucumber;
import cucumber.api.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
	plugin = { "pretty", "html:target/cucumber-html-report"},
	features = "src/test/resources"
)

public class TestRunner {}
```

The `@CucumberOptions` annotation sets the path where the feature files are located and configure the cucumber report file.
Let's execute the Test Runner and see what happens. Execute the TestRunner class as a JUnit test by right-clicking the file. You can also execute `mvn clean test` in the terminal. By the end you should see an output like this:

```
No feature found at [src/test/resources]

0 Scenarios
0 Steps
0m0.000s 
```

The TestRunner was executed and Cucumber output was returned, no feature files were found in our project, let's move on and create it.

## The Feature File

Both directories that will hold our project are empty at this point, create a feature file and save it under `src/test/resources/com.avenuecode`, name it Login.feature and write a scenario to test our application. At the end you should have something like this:

```gherkin
Feature: As an Avenue Code website user, I want to see 6 locations for job positions in Careers Page

Scenario: Successfully display job locations
	Given I visit Avenue Code website
	When I navigate to Open Positions page
	Then I should see at least three job locations
```

Now let's run our project again and check the output.


```
-------------------------------------------------------
 T E S T S
-------------------------------------------------------
Running com.avenuecode.TestRunner
Feature: As an Avenue Code website user, I want to see 6 locations for job positions in Careers Page
Starting ChromeDriver 2.25.426935 (820a95b0b81d33e42712f9198c215f703412e1a1) on port 15482
Only local connections are allowed.

  Scenario: Successfully display job locations      # com/avenuecode/login.feature:3
    Given I visit Avenue Code website
    When I navigate to Open Positions page
    Then I should see at least three job locations

1 Scenarios (1 undefined)
3 Steps (3 undefined)
0m1.538s


You can implement missing steps with the snippets below:

@Given("^I visit Avenue Code website$")
public void i_visit_Avenue_Code_website() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    throw new PendingException();
}

@When("^I navigate to Open Positions page$")
public void i_navigate_to_Open_Positions_page() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    throw new PendingException();
}

@Then("^I should see at least three job locations$")
public void i_should_see_at_least_three_job_locations() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    throw new PendingException();
}

Tests run: 5, Failures: 0, Errors: 0, Skipped: 4, Time elapsed: 1.91 sec

Results :

Tests run: 5, Failures: 0, Errors: 0, Skipped: 4
```

The cucumber output informed that we have one scenario and 3 undefined steps, that happens because we didn't create any step definitions for it yet. Let's create the steps definitions.

## Creating the Step Definitions

Create a CareerSteps class under `src/test/java/com/avenuecode`, copy the step definitions from the previous test run. Don't forget to add the imports to your file. In the end, you should have this code:

```java
package com.avenuecode;

import static org.junit.Assert.assertTrue;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;

public class CareerSteps {

	@Given("^I visit Avenue Code website$")
	public void i_visit_Avenue_Code_website() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
	}

	@When("^I navigate to Open Positions page$")
	public void i_navigate_to_Open_Positions_page() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
	}

	@Then("^I should see at least three job locations$")
	public void i_should_see_at_least_three_job_locations() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
	}
}

```

Now we have the step definitions, we can start adding the logic for each step from our scenario. First, we need to start a browser to begin our test. Add the following dependencies to the class:

```java
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
```

Now, instead of starting the browser from the step definitions we can use Cucumber `@Before` and `@After` hooks, that will execute the browser before running the tests and quit after the execution is finished and we will keep out of out step logic. Let's look at this code:

```java
package com.avenuecode;

import static org.junit.Assert.assertTrue;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;
import cucumber.api.java.Before;
import cucumber.api.java.After;

public class CareerSteps {
  WebDriver driver;

	@Before
	public void start() {
        driver = new ChromeDriver();
	}

	@After
	public void tearDown() {
	    driver.quit();
	}

	@Given("^I visit Avenue Code website$")
	public void i_visit_Avenue_Code_website() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
	}

	@When("^I navigate to Open Positions page$")
	public void i_navigate_to_Open_Positions_page() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
	}

	@Then("^I should see at least three job locations$")
	public void i_should_see_at_least_three_job_locations() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
	}
}

```

In line 13, we're declaring the driver variable that we'll use to start the browser. From lines 15 to 23 the Cucumber hooks were added, each with a method. The `start()` method will create a Chrome instance from Selenium WebDriver and the `tearDown()` method will close the browser once the tests finish. Also, we're importing the hooks class paths in lines 9 and 10.

Now we're ready to start adding the logic inside the steps. Remember that our first step needs to:
1. Open the Browser
2. Navigate to the website home page
Since we're already taking care of starting the browser in the `@Before` hook we just need to navigate to Avenue Code home page. Our first step should look like this:

```java
@Given("^I visit Avenue Code website$")
	public void i_visit_Avenue_Code_website() throws Throwable {
		driver.get("https://www.avenuecode.com");
	}
```

Now we can start filling out the other steps, we'll need to identify the elements in the page and map them in our code. We'll add a few imports to our class again since we'll want to locate the elements in the page:

```java
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
```

In the second step, we'll want to navigate to the Open Positions page. First, we need to navigate to Careers page by clicking a link then on Careers page we click another link to access the page we want.

```java
@When("^I navigate to Open Positions page$")
	public void i_navigate_to_Open_Positions_page() throws Throwable {
		WebElement careerLink = driver.findElement(By.linkText("Careers"));
		careerLink.click();
		WebElement viewOpenings = driver.findElement(By.linkText("VIEW ALL OPENINGS"));
		viewOpenings.click();
	}
```

Let's take a look at the code above. There are two variables, careerLink and viewOpenings that are of WebElement type, both contain the instructions to the WebDriver to find the element we want to click. After setting the WebElements we want they will work as a handler for a number of actions like `.click()`, `hover()`, `sendKeys()`, etc.

Now we write the last step for the `Then` clause and finish our scenario.

```java
@Then("^I should see at least three job locations$")
	public void i_should_see_at_least_three_job_locations() throws Throwable {
		Integer cityCount = driver.findElements(By.xpath("//div[@id='locationSelect']/div[@class='options']/div")).size();
		assertTrue(cityCount.intValue() >= 3);
	}
```

This time it is a bit different, our variable `cityCount` is an `Integer`, and `findElement` is in the plural form. That happens because  we want to get the number of common elements from each City container instead of checking every city element. The method `findElements` returns a Map containing the list of elements matching our criteria, we need just the size of that map do our assertion. Finally, we are using JUnit assertion library to compare if we have three cities or not.

Note also, that we used xpath as our element locator, when using xpath try to use a clean and small locator that will make it easier to maintain the code and avoid breaking it due to changes on the website.

By now you should be done, run your test and check the output. You should have an output like this:

```
-------------------------------------------------------
 T E S T S
-------------------------------------------------------
Running com.avenuecode.TestRunner
Feature: As an Avenue Code website user, I want to see 6 locations for job positions in Careers Page
Starting ChromeDriver 2.25.426935 (820a95b0b81d33e42712f9198c215f703412e1a1) on port 36497
Only local connections are allowed.

  Scenario: Successfully display job locations      # com/avenuecode/login.feature:3
    Given I visit Avenue Code website              # CareerSteps.i_visit_Avenue_Code_website()
    When I navigate to Open Positions page         # CareerSteps.i_navigate_to_Open_Positions_page()
    Then I should see at least three job locations # CareerSteps.i_should_see_at_least_three_job_locations()

1 Scenarios (1 passed)
3 Steps (3 passed)
0m16.322s
```

The final code containing all three step definitions will be:

```java
package com.avenuecode;

import static org.junit.Assert.assertTrue;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;
import cucumber.api.java.Before;
import cucumber.api.java.After;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class CareerSteps {
  WebDriver driver;

	@Before
	public void start() {
		driver = new ChromeDriver();
	}

	@After
	public void tearDown() {
		driver.quit();
	}

	@Given("^I visit Avenue Code website$")
	public void i_visit_Avenue_Code_website() throws Throwable {
		driver.get("https://www.avenuecode.com");
	}

	@When("^I navigate to Open Positions page$")
	public void i_navigate_to_Open_Positions_page() throws Throwable {
		WebElement careerLink = driver.findElement(By.linkText("Careers"));
		careerLink.click();
		WebElement viewOpenings = driver.findElement(By.linkText("VIEW ALL OPENINGS"));
		viewOpenings.click();
	}

	@Then("^I should see at least three job locations$")
	public void i_should_see_at_least_three_job_locations() throws Throwable {
		Integer cityCount = driver.findElements(By.xpath("//div[@id='locationSelect']/div[@class='options']/div")).size();
		assertTrue(cityCount.intValue() >= 3);
	}
```

## Page Object Model

Now we'll change our project to use the Page Object model, it has become a popular pattern and can ease code maintenance avoid writing duplicate methods. If you have doubts about what is a Page Object, think of it as a class that contains different objects that work as a wrapper and represent part of the web application being tested.
In order to add Page Object support to our framework we could either write our own page objects by adding some Java classes or using a library to do it. We'll go with the second option and use PageFactory, a library that will help to create our page objects.

### Changing the Project Structure

Let's start modifying our project structure to accommodate the changes. First, create a new package under src/test/java, name it com.avenuecode.pages. You should have something like this:

![alt text](images/10-page-object-tree.png)

### Creating the Page Objects
Now it is necessary to add our page objects. A reminder that the page objects doesn't necessarily need to be for a specific page, you can create a page object like a footer, header from a page or anything that we want to wrap together. We'll create two page objects, one for the Home page and another for the Careers page. Let's start with the home page. Inside the new package create a class named HomePage. Let's see what we need to code there:

```java
package com.avenuecode.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage {

}
```

Start by adding the imports, then we need to map the same WebElements that we used in our second step definition, "When I navigate to Open Positions page". The code for the Home Page class will look like this:

```java
package com.avenuecode.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage {
	
	@FindBy(linkText = "Careers")
	private WebElement careerLink;
	
	public void navigateToCareersPage(){
		careerLink.click();
	}
}
```

Note that the `@FindBy` annotation is used to declare the page objects. Likewise in Selenium's FindElement method, we can use different locators like ID, xpath, name, etc. Also, we're creating a method to navigate to Careers Page, since we're declaring all our page objects as private, then we'll allow external access only through the methods that will contain the actions. This is a design pattern, for more info take a look at the Reference at the end of this tutorial.

We will also need to create another page object to access the Open Positions link inside the Careers Page, let's create a new page object with a method to click the link. The final code should look like this: 

```java
package com.avenuecode.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CareerPage {
	@FindBy(linkText = "VIEW ALL OPENINGS")
	private WebElement viewOpenings; 
	
	public void navigateToAllOpeningsPage(){
		viewOpenings.click();
	}
}
```

Now let's go to our step definitions file and change it to use the first page object. In CareerSteps file, add the following imports:

```java
import org.openqa.selenium.support.PageFactory;
import com.avenuecode.pages.HomePage;
import com.avenuecode.pages.CareerPage;
```

One is the PageFactory library, the other is our first Page Object class. Now to the step definition let's change it by initializing our page object then calling the method we created. The code should look like this:

```java
@When("^I navigate to Open Positions page$")
	public void i_navigate_to_Open_Positions_page() throws Throwable {
		HomePage homePage = PageFactory.initElements(driver, HomePage.class);
	    homePage.navigateToCareersPage();
	    CareerPage careerPage = PageFactory.initElements(driver, CareerPage.class);
	    careerPage.navigateToAllOpeningsPage();
	}
```

In the first line of the method we initialized the Page Object and in the second line we're calling the method to navigate to Careers Page. In lines 3 and 4 we're doing the same but to access Open Positions Page. At this point you may want to run your test to make sure everything is working.

Now to finish the change, let's create the third Page Object class, create a class OpenPositionsPage under com.avenuecode.pages. Remember that we'll want to keep the private WebElements, so let's map the element then create a method to get the number of occurrences of the element. Your code should look like this:

```java
package com.avenuecode.pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;

public class OpenPositionsPage {
	
	@FindBys({
		@FindBy(xpath = "//div[@id='locationSelect']/div[@class='options']/div")
	})
	List<WebElement> cityItems;
	
	public int cityTotalCount(){
		int totalCount = cityItems.size();
		return totalCount;
	}
}
```

Note that the annotation is different this time, we need to use `@FindBys` to return a list containing all the matches for the `@FindBy` we have declared, from that we can get the elements count that matches the locator. Also, our variable will be a list of WebElements, that required to add a new import `java.util.List`. The method created return an int variable to be used in our assertion in the step definition. Moving back to the step definitions file, change it to initialize the Open Ppositions Page and use it to replace the current code.

```java
@Then("^I should see at least three job locations$")
	public void i_should_see_at_least_three_job_locations() throws Throwable {
		OpenPositionsPage openPositionsPage = PageFactory.initElements(driver, OpenPositionsPage.class);
		int currentQuantity = openPositionsPage.cityTotalCount();
		assertTrue(currentQuantity >= 3);
	}
```

### Improving the assertions

Our assertion for the job locations quantity works fine, but if it fails it's not telling us why it failed, we will just now that some comparison failed. To solve that and an assertion that has the same intent of the test we're doing let's use [Hamcrest](hamcrest.org).

Add hamcrest dependency to the pom file:

```xml
<dependency>
  		<groupId>org.hamcrest</groupId>
  		<artifactId>hamcrest-library</artifactId>
  		<version>1.3</version>
</dependency>
```

Build the project to get the new dependencies and add the imports to our steps file:

```java
import static org.hamcrest.MatcherAssert.assertThat; 
import static org.hamcrest.Matchers.*;
```

Now we can use Hamcrest assertions in the code. Start by changing our assertion for this one:

```java
assertThat(currentQuantity, greaterThanOrEqualTo(3));
```
Note that now we're testing if the `currentQuantity` is greater than 3 locations, the test it is easier and if it fails it will be too.  

To finish, try to execute your code and check if everything works, the steps file should look like this:

```java
package com.avenuecode;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;
import cucumber.api.java.Before;
import cucumber.api.java.After;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;

import com.avenuecode.pages.CareerPage;
import com.avenuecode.pages.HomePage;
import com.avenuecode.pages.OpenPositionsPage;

import static org.hamcrest.MatcherAssert.assertThat; 
import static org.hamcrest.Matchers.*;

public class CareerSteps {
	WebDriver driver;
	
	@Before
	public void start() {
		driver = new ChromeDriver();
	}
	
	@After
	public void tearDown() {
		driver.quit();
	}
	
	@Given("^I visit Avenue Code website$")
	public void i_visit_Avenue_Code_website() throws Throwable {
		driver.get("https://www.avenuecode.com");
	}

	@When("^I navigate to Open Positions page$")
	public void i_navigate_to_Open_Positions_page() throws Throwable {
		HomePage homePage = PageFactory.initElements(driver, HomePage.class);
	    homePage.navigateToCareersPage();
	    CareerPage careerPage = PageFactory.initElements(driver, CareerPage.class);
	    careerPage.navigateToAllOpeningsPage();
	}

	@Then("^I should see at least three job locations$")
	public void i_should_see_at_least_three_job_locations() throws Throwable {
		OpenPositionsPage openPositionsPage = PageFactory.initElements(driver, OpenPositionsPage.class);
		int currentQuantity = openPositionsPage.cityTotalCount();
	    assertThat(currentQuantity, greaterThanOrEqualTo(3));
	}
}
```

## Exercises
1. **Refactor Job Locations Scenario:** Probably, you have a framework that can execute one scenario but still there's room for improvement. Our requirement to have at least three job locations can be changed by product owner without notice. To keep the code cleaner refactor your scenario by changing the last step to allow you to change the quantity in the feature file without the need to change the values inside the step definition file.   
2. **Feature 1:** As a user I want the footer to display the top three News, when a link is clicked it should redirect to the content.
3. **Feature 2:** As a user I want the footer to display the latest event, when a event is clicked it should redirect to the Events page displaying the modal with the event details.
4. **Feature 3:** As a user I want to filter the next events section by location, the locations should be: San Francisco, Belo Horizonte and S�o Paulo.
5. **CHALLENGE:** As a user I want the Home Page header to display the following links: Home, Who We Are, What We Do, Portfolio, Partners, Careers. In Portuguese version it should display: Home, Quem Somos N�s, O Que Fazemos, Portf�lio, Parceiros, Carreiras.

## References
*Java:*
* http://www.tutorialspoint.com/software_testing_dictionary/assertion_testing.htm
* http://junit.sourceforge.net/javadoc/

*Maven:*
* https://maven.apache.org/pom.html

*Selenium:*
* http://seleniumhq.github.io/selenium/docs/api/java/  
* http://testerstories.com/2015/10/using-cucumber-jvm-with-selenium-webdriver/
* http://testerstories.com/2015/10/page-objects-with-selenium-and-cucumber-jvm/
* http://www.seleniumhq.org/docs/03_webdriver.jsp

*Page Object:*
* http://toolsqa.com/selenium-webdriver/page-object-pattern-model-page-factory/
* http://docs.seleniumhq.org/docs/06_test_design_considerations.jsp#page-object-design-pattern
* http://toolsqa.com/selenium-webdriver/page-object-model/
* https://github.com/SeleniumHQ/selenium/wiki/PageFactory
* http://relevantcodes.com/pageobjects-and-pagefactory-design-patterns-in-selenium/

*xpath*
* http://www.tothenew.com/blog/best-practices-of-choosing-good-locators-for-your-test-automation-scripts/

## Pitfalls (do the exercices first)
### Sharing step definitions
Suppose that we have a step definition like this:

```
public class CareersSteps extends StepDefinition {

    WebDriver driver;

    @Before
    public void setUp() {
        driver = new ChromeDriver();
    }

    @When("^I navigate to Open Positions page$")
    public void i_navigate_to_Open_Positions_page() throws Throwable {
        HomePage homePage = PageFactory.initElements(driver, HomePage.class);
        homePage.navigateToCareersPage();
        CareersPage careerPage = PageFactory.initElements(driver, CareersPage.class);
        careerPage.navigateToAllOpeningsPage();
    }

    @Then("^I should see at least \"([^\"]*)\" job locations$")
    public void i_should_see_at_least_job_locations(int jobLocationsAmount) throws Throwable {
        OpenPositionsPage openPositionsPage = PageFactory.initElements(driver, OpenPositionsPage.class);
        assertThat(openPositionsPage.cityTotalCount(), greaterThanOrEqualTo(jobLocationsAmount));
    }
    
    @After
    public void turnDown() {
        driver.quit();
    }
}
```

This step definition above maps to the following feature file:

```
Feature: As a Avenue Code website user, I want to see 6 locations for job positions in Careers Page

    Scenario: Succesfully display job locations
        Given I visit Avenue Code website
        When I navigate to Open Positions page
        Then I should see at least "3" job locations
```

As we can see, the step ***Given** I visit Avenue Code website* it's not implemented in our *CareersPage* step definition
class and even though we have it specified in our feature file. Well, you guessed right, we are reusing it! 
Cucumber treats every step definition as a unique step (regex), so it will match this step in our feature file to some 
implementation in some other step definition (Cucumber will find it if it exists).

Suppose that this step definition that implemented this step that im referencing in my feature file is the following below:

```
public class CommonSteps extends StepDefinition {

    WebDriver driver;
    
    @Before
    public void setUp() {
        driver = new ChromeDriver();
    }

    @Given("^I visit Avenue Code website$")
    public void i_visit_Avenue_Code_website() throws Throwable {
        driver.get("https://www.avenuecode.com/");
    }
    
    @After
    public void turnDown() {
        driver.quit();
    }
}
```

If you do something like this you will receive a big error message saying that it could not found
the elements or your assertion just failed.

Well, let's understand why this is happening!

We are creating a new instance (using the Cucumber JVM *@Before* hook) before each scenario in each
of our steps classes. So each class (step definition) have their own webdriver instance that it's refreshed
before every scenario (*startup*).

So if those classes have their independent webdriver, how can we reuse the step definition 
from the *CommonSteps* like in our example above? The drive referenced in the implementation of 
that step belongs to the *CommonSteps* class, not the *CareersSteps*. This way, I'm never going to 
visit the Avenue Code website... actually, *CommonSteps* will do it (not what we want).

*You might have thought for a second that Cucumber would give us the step definition 
(that belongs to other step definition class) and like in JavaScript (**this** keyword), 
this way, the driver referenced inside this step definition would belong to the callee context (the 
driver would belong to the context of the class that called this method) and this context would be the 
CareersSteps (what we wanted), but this is not JavaScript, this is Java! **We need to share the same driver 
instance in order to accomplish that sharing and we can do this by moving the instance one level up, put it
on an abstract class as static, so every step definition class would be inheriting the same class and talking
about the same reference driver**.*

Let's explore the solution using the abstract class approach:
```
package com.avenuecode;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public abstract class StepDefinition {
    protected static WebDriver driver;

    protected WebDriver getDriver() {
        if (driver == null) {
            driver = new ChromeDriver();
        }
        return driver;
    }

    protected WebDriver quitDriver() {
        if (driver != null) {
            driver.quit();
        }
        return driver = null;
    }
}
```
Now we have a class that every step definition will inherit from, the webdriver instance is now static and 
everybody have have access to it by calling the *getDriver* method from the abstract base class. The method
will return the instance if its not null or create a new one if it is. 

You can see that there is a *quitDriver* method also... this method is responsible for quitting the webdriver
if there is one. We could just quit the driver instead of checking for null but the problem with this is that
quitting the driver doesn't make it null, and if we don't do this when other step definition call getDriver, the
getDriver will return the driver that it's not null but its quitted, and this will break our tests because we
are trying to use a quitted webdriver (it's not null, it's like a webdriver corpse). We could also have implemented
the *@After* hook in each step definition class to quit the webdriver after each scenario but as we have an abstract 
class and this class holds the webdriver instance, its his responsibility to quit it.  
```
package com.avenuecode;

import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import com.avenuecode.pages.HomePage;
import com.avenuecode.pages.CareersPage;
import com.avenuecode.pages.OpenPositionsPage;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class CareersSteps extends StepDefinition {

    WebDriver driver = getDriver();

    @When("^I navigate to Open Positions page$")
    public void i_navigate_to_Open_Positions_page() throws Throwable {
        HomePage homePage = PageFactory.initElements(driver, HomePage.class);
        homePage.navigateToCareersPage();
        CareersPage careerPage = PageFactory.initElements(driver, CareersPage.class);
        careerPage.navigateToAllOpeningsPage();
    }

    @Then("^I should see at least \"([^\"]*)\" job locations$")
    public void i_should_see_at_least_job_locations(int jobLocationsAmount) throws Throwable {
        OpenPositionsPage openPositionsPage = PageFactory.initElements(driver, OpenPositionsPage.class);
        assertThat(openPositionsPage.cityTotalCount(), greaterThanOrEqualTo(jobLocationsAmount));
    }
}
```

```
package com.avenuecode;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

import org.openqa.selenium.WebDriver;

public class CommonSteps extends StepDefinition {

    WebDriver driver = getDriver();

    @Given("^I visit Avenue Code website$")
    public void i_visit_Avenue_Code_website() throws Throwable {
        driver.get("https://www.avenuecode.com/");
    }

    @Then("^I close the browser$")
    public void i_close_the_browser() throws Throwable {
        quitDriver();
    }
}
```
